# Hackathon Leerstand

Informationen zu dem Hackathon zum Thema Leerstandsmanagement vom FabLab Allgäeu. e.V. in Kooperation mit der Stadt Kempten.

## Setup

```bash
$ git clone https://gitlab.com/fablab-allgaeu/hackathon-leerstand
```

### Als Entwicker

#### Backend

Installieren von Python: https://www.python.org/downloads/


In einer Shell django starten:
```bash
$ cd backend
$ cp dev.env .env

# modify your settings here
$ nano .env

$ python3 -m pip install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
$ python3 manage.py runserver
```

#### Frontend
Installieren von node: https://nodejs.org/en/download/

```bash
$ cd frontend
$ npm install
$ npm start
```

#### Links
| Link | Beschreibung |
| --- | --- |
|http://localhost:3000/|Anwendung|
|http://localhost:8000/admin/|Django Admin Seite|
|http://localhost:8000/api/schema/swagger-ui/|Swagger UI|

### Via docker-compose

```bash
$ docker-compose build
$ docker-compose up -d
$ docker-compose exec backend python3 manage.py migrate
```
Die Anwendung ist jetzt unter dem Link http://localhost:8888 erreichbar.

## Testing

### Selenium IDE
Mit dem Selenium IDE Browerplugin können Benutzerabfolgen im Brower
aufgezeichnet, bearbeitet und wieder abgespielt werden.
https://addons.mozilla.org/de/firefox/addon/selenium-ide/
