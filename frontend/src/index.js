import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import './index.css';
import { Route, BrowserRouter as Router, Routes, Navigate} from 'react-router-dom';
import App from './App';
import Register from './components/register';
import Login from './components/login';
import Logout from './components/logout';
import Activate from './components/activate';
import NewPost from './components/newPost';
import { RequestPasswortReset, PasswortReset } from './components/passwort-reset';

const routing = (
	<Router>
		<React.StrictMode>
			<Routes>
				<Route exact path="/" element={<App />} />
				<Route path="/register" element={<Register />} />
				<Route path="/login" element={<Login />} />
				<Route path="/logout" element={<Logout />} />
				<Route path="/activate" element={<Activate />} />
				<Route path="/request-password-reset" element={<RequestPasswortReset />} />
				<Route path="/password-reset" element={<PasswortReset />} />
				<Route path="/new-post" element={<NewPost />} />
				<Route path="/*" element={<Navigate replace to="/"/>} />
			</Routes>
		</React.StrictMode>
	</Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
