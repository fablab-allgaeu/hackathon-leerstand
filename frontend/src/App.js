import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import './App.css';
import Posts from './components/posts';
import CommonPage from './components/CommonPage'
import PostLoadingComponent from './components/postLoading';
import axiosInstance from './axios';
import { isUserLoggedIn } from './components/utils';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';

function App() {
	const PostLoading = PostLoadingComponent(Posts);
	const [appState, setAppState] = useState({
		loading: true,
		posts: null,
	});

	const reloadPosts = () => {
		axiosInstance.get().then((res) => {
			const allPosts = res.data;
			setAppState({ loading: false, posts: allPosts });
			console.log(res.data);
		})
			.catch(error => console.error(error));

	}
	useEffect(() => {
		reloadPosts();
	}, [setAppState]);
	return (
		<CommonPage title="Blog">
			<h1>Latest Posts</h1>
			<PostLoading isLoading={appState.loading} posts={appState.posts} reloadPosts={reloadPosts} />
			{isUserLoggedIn ?
				<Link to="/new-post">
					<Fab sx={{
						position: "fixed",
						bottom: (theme) => theme.spacing(3),
						right:(theme) =>  theme.spacing(3),
						}}
						color="primary" aria-label="add">
						<AddIcon />
					</Fab>
				</Link>
				: <></>}
		</CommonPage>
	);
}
export default App;
