import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {isUserLoggedIn} from './utils';


import AppBarButton from './AppBarButton';

function CustomAppBar(props) {
    const hasBackHandler = props.hasOwnProperty("backHandler") && (typeof props.backHandler !== "undefined");
    const navigate = useNavigate();

    return (
        <AppBar position="static">
          <Toolbar stype={{flex: 1}}>
            {hasBackHandler ?
            <IconButton
              size="large"
              edge="start"            
              color="inherit"
              sx={{mr: 2}}
              onClick={props.backHandler}
            >
              <ArrowBackIcon />
            </IconButton>
            :
            <></>
            }
            <Typography variant="h6" component="div" style={{flex: 1}}>
            {props.title}
            </Typography>
					{isUserLoggedIn()?
						<Container style={{margin: 20, display: "flex", align: "right", justifyContent: "flex-end", flex: 2}}>
              <AppBarButton onClick={() => navigate("/register")} name="Register"></AppBarButton>
              <AppBarButton onClick={() => navigate("/login")} name="Login"></AppBarButton>
						</Container>
						:
              <AppBarButton onClick={() => navigate("/logout")} name="Logout"></AppBarButton>
					}
          </Toolbar>
        </AppBar>
    );
}

export default CustomAppBar;