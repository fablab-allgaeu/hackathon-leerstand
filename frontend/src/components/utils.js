
export function isUserLoggedIn() {
    return localStorage.getItem('access_token') === null;
}