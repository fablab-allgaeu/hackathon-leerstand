import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

import CommonPage from './CommonPage';
import axiosInstance from '../axios';

export default function SignUp() {
	const navigate = useNavigate();
	const initialFormData = Object.freeze({
		email: '',
		username: '',
		password: '',
	});

	const [formData, updateFormData] = useState(initialFormData);
	const [usernameExists, setUsernameExists] = useState(false);
	const [emailExists, setEmailExists] = useState(false);
	const theme = createTheme();

	const handleChange = (e) => {
		updateFormData({
			...formData,
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		axiosInstance
			.post(`user/create/`, {
				email: formData.email,
				user_name: formData.username,
				password: formData.password,
			})
			.then((res) => {
				navigate('/login');
				console.log(res);
				console.log(res.data);
			})
			.catch((error) => {
				const msg = error.response.data.msg;
				const usernameError = msg === 'username already exists';
				const emailError = msg === 'email already exists';
				if (usernameError || emailError) {
					setEmailExists(emailError);
					setUsernameExists(usernameError);
				} else {
					throw error;
				}
			});
	};


	return (
		<ThemeProvider theme={theme}>
			<CommonPage title="Register User">
				<CssBaseline />
				<Box sx={{
					marginTop: 8,
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}>
					<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign up
					</Typography>
					<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
						<TextField
							variant="outlined"
							required
							fullWidth
							id="email"
							label="Email Address"
							name="email"
							margin="normal"
							autoComplete="email"
							autoFocus
							onChange={handleChange}
							error={emailExists}
							helperText={emailExists ? "Email already exists" : null}
						/>
						<TextField
							variant="outlined"
							required
							fullWidth
							id="username"
							label="Username"
							name="username"
							margin="normal"
							autoComplete="username"
							onChange={handleChange}
							error={usernameExists}
							helperText={usernameExists ? "Username already exists" : null}
						/>
						<TextField
							variant="outlined"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							margin="normal"
							autoComplete="current-password"
							onChange={handleChange}
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							sx={{ mt: 3, mb: 2 }}
							onClick={handleSubmit}
						>
							Sign Up
						</Button>
						<Grid container>
							<Grid item>
								<Link to="/login">
									Already have an account? Sign in
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</CommonPage>
		</ThemeProvider>
	);
}
