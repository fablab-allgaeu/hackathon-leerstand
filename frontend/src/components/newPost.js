
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';

import axiosInstance from '../axios';
import CommonPage from './CommonPage';

export default function NewPosts() {
	const navigate = useNavigate();
	const initialFormData = Object.freeze({
		title: null,
		content: null,
	});
	const [formData, updateFormData] = useState(initialFormData);
	const theme = createTheme();

	const handleChange = (e) => {
		updateFormData({
			...formData,
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		console.log(formData);

		axiosInstance
			.post(``, {
				title: formData.title,
				content: formData.content,
      //  author: 1,
        category: 1,
			})
			.then(() => {
				navigate('/');
			})
			.catch((error) => {
          console.error(error);
			    alert(error);
			});
	};
    return (
		<ThemeProvider theme={theme}>
			<CommonPage title="Add New Post">
				<CssBaseline />
				<Box sx={{
					marginTop: 8,
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'left',
				}}>
					<Typography component="h1" variant="h5">
						Add New Post
					</Typography>
					<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="title"
							label="Title"
							name="title"
							autoFocus
							onChange={handleChange}
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="content"
							label="Content"
							id="content"
                            multiline
                            rows={5}
							onChange={handleChange}
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							sx={{mt: 3, mb: 2}}
							onClick={handleSubmit}
                            disabled={!(formData.title && formData.content)}
						>
							Create New Post
						</Button>
					</Box>
				</Box>
			</CommonPage>
		</ThemeProvider>
    );
}
