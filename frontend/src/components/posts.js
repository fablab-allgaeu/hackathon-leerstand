import React from 'react';
import { useNavigate } from 'react-router-dom';

import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { alpha } from '@mui/material/styles';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import axiosInstance from '../axios';


const Posts = (props) => {
	const { posts, reloadPosts } = props;
	const navigate = useNavigate();

	const [ deleteId, setDeleteId ] = React.useState(null);
	const [open, setOpen] = React.useState(false);

	if (!posts || posts.length === 0) return <p>Can not find any posts, sorry</p>;

	const handleClickOpen = (id) => {
		setDeleteId(id);
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
		setDeleteId(null);
	};


	function handleSubmit() {
		axiosInstance
			.delete("/" + deleteId, {
			})
			.then(() => {
				navigate('/');
        			reloadPosts();
			})
			.catch((error) => {
				console.error(error);
				alert(error);
			});
		handleClose();
	}

	return (
		<Grid container spacing={2} sx={{ margin: 0, widht: "100%" }}>
			{posts.map((post) => {
				return (
					<Grid item xs={12} md={6}>
						<Box
							sx={{
								display: 'flex',
								flexDirection: { xs: 'column', md: 'row' },
								alignItems: 'center',
								bgcolor: 'background.paper',
								overflow: 'hidden',
								borderRadius: '12px',
								boxShadow: 1,
								flexGrow: 4,
								margin: "12px",
								fontWeight: 'bold'
							}}
						>
							<Box
								component="img"
								sx={{
									height: 233,
									width: 350,
									maxHeight: { xs: 233, md: 167 },
									maxWidth: { xs: 350, md: 250 },
								}}
								alt={post.title}
								src="https://images.unsplash.com/photo-1512917774080-9991f1c4c750?auto=format&w=350&dpr=2"
							/>
							<Box
								sx={{
									display: 'flex',
									flexDirection: 'column',
									alignItems: { xs: 'center', md: 'flex-start' },
									m: 3,
									minWidth: { md: 350 },
								}}
							>
								<Box component="span" sx={{ fontSize: 16, mt: 1 }}>
									{post.author}
								</Box>
								<Box component="span" sx={{ color: 'primary.main', fontSize: 22 }}>
									{post.title}
								</Box>
								<Box
									sx={{
										mt: 1.5,
										p: 0.5,
										backgroundColor: (theme) => alpha(theme.palette.primary.main, 0.1),
										borderRadius: '5px',
										color: 'primary.main',
										fontWeight: 'medium',
										display: 'flex',
										fontSize: 12,
										alignItems: 'center',
										'& svg': {
											fontSize: 21,
											mr: 0.5,
										},
									}}
								>
									<ErrorOutlineIcon />
									{post.content}
								</Box>
								<Button
									type="submit"
									variant="contained"
									color="primary"
									sx={{ marginTop: (theme) => theme.spacing(1) }}
									onClick={() => handleClickOpen(post.id)}
									startIcon={<DeleteIcon />}
								>
									delete
								</Button>
							</Box>

						</Box>
					</Grid>)
			})}
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">
					{"Delete this post?"}
				</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						Do you really want to delete this post?
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="inherit">Abort</Button>
					<Button onClick={handleSubmit} autoFocus>
						Yes delete post
					</Button>
				</DialogActions>
			</Dialog>
		</Grid>
	);
};
export default Posts;
