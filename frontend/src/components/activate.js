import React, { useEffect, useState } from 'react';
import axiosInstance from '../axios';
import { useNavigate } from 'react-router-dom';

import CircularProgress from '@mui/material/CircularProgress';
import CommonPage from './CommonPage';

export default function Activate(props) {
	const navigate = useNavigate();
	const [activationDone, setAcivationDone] = useState(false);


  useEffect(() => {
    const query = new URLSearchParams(props.location.search);
    console.log(query.get("token"))
		axiosInstance
			.get(`user/activate`, { params: {
        token: query.get("token"),
        uibd64: query.get("uid"),
			}})
			.then((res) => {
        setAcivationDone(true);
				setTimeout(() => navigate('/login'), 3000);
			});

  },[props.location.search, navigate]);


	return(
    <CommonPage title="User activation">
    {!activationDone ? <>
      <h1>Activation in progress</h1>
      <CircularProgress />
      </>
      :
      <h1>Activation done, redirect to login page</h1>
    }
    </CommonPage>
    );
}
