import React from 'react';
import CustomAppBar from './CustomAppBar';
import Footer from './Footer';
import Container from '@mui/material/Container';

export default function CommonPage({children, title, backHandler}) {
    return (
    <div style={{display: "flex", flexDirection: "column", height:"100vh", justifyContent: "space-between", widht: "100%", margin: 0}}>
      <CustomAppBar title={title} backHandler={backHandler}/>
		    <Container component="main">
          {children}
        </Container>
      <Footer />
    </div>
    );
}