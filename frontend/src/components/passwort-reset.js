import React, { useState } from 'react';

import { useNavigate, Link } from 'react-router-dom';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

import CommonPage from './CommonPage';
import axiosInstance from '../axios';

export function RequestPasswortReset() {
	const navigate = useNavigate();
	const initialFormData = Object.freeze({
		email: '',
	});
	const theme = createTheme();
	const [formData, updateFormData] = useState(initialFormData);

	const handleChange = (e) => {
		updateFormData({
			...formData,
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		axiosInstance
			.post(`user/reset-password/`, {
				email: formData.email,
			})
			.then((res) => {
				navigate('/login');
			});
	};

	return (
		<ThemeProvider theme={theme}>
			<CommonPage title="Request Passwort Reset">
				<CssBaseline />
				<Box sx={{
					marginTop: 8,
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}>
					<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}></Avatar>
					<Typography component="h1" variant="h5">
						Request passwort reset
					</Typography>
					<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
						<TextField
							variant="outlined"
							required
							fullWidth
							id="email"
							margin="normal"
							label="Email Address"
							name="email"
							autoComplete="email"
							autoFocus
							onChange={handleChange}
						/>

						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							sx={{ mt: 3, mb: 2 }}
							onClick={handleSubmit}
						>
							Request Password reset
						</Button>
						<Grid container justify="flex-end">
							<Grid item>
								<Link to="/login">
									Sign in
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</CommonPage>
		</ThemeProvider>
	);
}

export function PasswortReset(props) {
	const navigate = useNavigate();
	const initialFormData = Object.freeze({
		password: '',
		password_confirm: '',
	});

	const [formData, updateFormData] = useState(initialFormData);
	const [message, setMessage] = useState("");

	const handleChange = (e) => {
		updateFormData({
			...formData,
			// Trimming any whitespace
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		const query = new URLSearchParams(props.location.search);

		axiosInstance
			.post(`user/reset-password/confirm/`, {
				password: formData.password,
				token: query.get('token'),
			})
			.then((res) => {
				navigate('/login');
			})
			.catch((error) => {
				const response = error.response.data;
				if (response.detail === 'Not found.') {
					setMessage("invalid token for password reset");
				} else {
					setMessage(JSON.stringify(response));
				}
			});
	};

	/*
	const classes = useStyles();
	*/

	return (
		<CommonPage title="User Password Reset">
			<CssBaseline />
			<div /*className={classes.paper}*/>
				{message === "" ?
					<></> :
					<Typography component="h2" variant="h5">
						{message}
					</Typography>
				}
				<Avatar /*className={classes.avatar}*/></Avatar>
				<Typography component="h1" variant="h5">
					Request passwort reset
				</Typography>
				<form /*className={classes.form}*/ noValidate>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						name="password"
						label="Password"
						type="password"
						id="password"
						autoComplete="current-password"
						onChange={handleChange}
					/>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						name="password_confirm"
						label="Password confirm"
						type="password"
						id="password confirm"
						autoComplete="current-password"
						onChange={handleChange}
					/>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						/*className={classes.submit}*/
						onClick={handleSubmit}
						disabled={formData.password === "" || formData.password !== formData.password_confirm}
					>
						Request Password reset
					</Button>
					<Grid container justify="flex-end">
						<Grid item>
							<Link href="/login" variant="body2">
								Sign in
							</Link>
						</Grid>
					</Grid>
				</form>
			</div>
		</CommonPage>
	);
}
