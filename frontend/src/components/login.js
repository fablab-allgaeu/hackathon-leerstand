import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

import axiosInstance from '../axios';
import CommonPage from './CommonPage';

export default function SignIn() {
	const navigate = useNavigate();
	const initialFormData = Object.freeze({
		email: '',
		password: '',
	});
	const [loginError, setLoginError] = useState(null)
	const [formData, updateFormData] = useState(initialFormData);
	const theme = createTheme();

	const handleChange = (e) => {
		updateFormData({
			...formData,
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		console.log(formData);

		axiosInstance
			.post(`token/`, {
				email: formData.email,
				password: formData.password,
			})
			.then((res) => {
				localStorage.setItem('access_token', res.data.access);
				localStorage.setItem('refresh_token', res.data.refresh);
				axiosInstance.defaults.headers['Authorization'] =
					'JWT ' + localStorage.getItem('access_token');
				setLoginError(null);
				navigate('/');
			})
			.catch((error) => {
				if (
					error.response.status === 401 &&
					error.response.statusText === "Unauthorized" &&
					error.response.data.detail === "No active account found with the given credentials"
				) {
					setLoginError("Invalid credentials")
				} else {
					console.error(error)
				}
			});
	};

	return (
		<ThemeProvider theme={theme}>
			<CommonPage title="User Login">
				<CssBaseline />
				<Box sx={{
					marginTop: 8,
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}>
					<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="email"
							label="Email Address"
							name="email"
							autoComplete="email"
							autoFocus
							onChange={handleChange}
							error={loginError !== null}
							helperText={loginError}
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
							onChange={handleChange}
							error={loginError !== null}
							helperText={loginError}
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							sx={{mt: 3, mb: 2}}
							onClick={handleSubmit}
						>
							Sign In
						</Button>
						<Grid container>
							<Grid item xs>
								<Link to="/register">
									Don't have an account? Sign Up
								</Link>
							</Grid>
							<Grid item>
								<Link to="/request-password-reset">
									Forgot pasword
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</CommonPage>
		</ThemeProvider>
	);
}
