import React from 'react';
import { Link } from 'react-router-dom';

import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

export default function Footer() {
  return (
    <div style={{width: "100%"}}>
      <Divider />
      <Typography align="center" variant="body1" component="div">Entstanden im Kreise des <a href="https://fablab-allgaeu.de/">FabLab Allgäu e.V.</a></Typography>
      <Typography align="center" variant="body1" component="div">
          <Link to="/datenschutz">Datenschutz</Link> - <Link to="/impressum">Impressum</Link>
      </Typography>
    </div>)
}