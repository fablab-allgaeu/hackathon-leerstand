import Button from '@mui/material/Button';

function AppBarButton(props) {
    const buttonStyle = {margin: "0 10px"};

    return (
        <Button variant="outlined" color="inherit" style={buttonStyle} onClick={() => props.onClick()}>{props.name}</Button>
    );
}

export default AppBarButton;