from datetime import timedelta

from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.db import IntegrityError
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django_rest_passwordreset.models import ResetPasswordToken
from django_rest_passwordreset.signals import reset_password_token_created
from django_rest_passwordreset.views import get_password_reset_token_expiry_time
from drf_spectacular.utils import extend_schema, OpenApiParameter
from requests import PreparedRequest
from rest_framework import parsers, renderers, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from core import settings
from .models import NewUser
from .serializers import CustomTokenSerializer
from .serializers import CustomUserSerializer
from .tokens import account_activation_token


class CustomUserCreate(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format='json', url_parse=None):
        serializer = CustomUserSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = serializer.save()
            except IntegrityError as e:
                print(e)
                if str(e) == "UNIQUE constraint failed: users_newuser.email":
                    user = NewUser.objects.get(email=serializer.data['email'])
                    if user:
                        json = {
                            "msg": "email already exists"
                        }
                    return Response(json, status=status.HTTP_400_BAD_REQUEST)

                if str(e) == "UNIQUE constraint failed: users_newuser.user_name":
                    user = NewUser.objects.get(user_name=serializer.data['user_name'])
                    if user:
                        json = {
                            "msg": "username already exists"
                        }
                    return Response(json, status=status.HTTP_400_BAD_REQUEST)
                else:
                    json = {
                        "msg": "unkown activation error"
                    }
                    return Response(json, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            if user:
                activation_site = "{}/activate".format(settings.SITE_URL)
                mail_subject = 'Activate your account.'
                params = {
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }
                req = PreparedRequest()
                req.prepare_url(activation_site, params)
                url = req.url
                message = render_to_string('blog/acc_active_email.html', {
                    'username': user.user_name,
                    'activation_link': url,
                })
                to_email = user.email
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()

                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BlacklistTokenUpdateView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = ()

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ActivateTokenView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = ()

    class Meta:
        fiels = ['bak']

    @extend_schema(
        parameters=[
            OpenApiParameter(name='uibd64', location=OpenApiParameter.QUERY, description='uibd64 for user activation',
                             required=True, type=str),
            OpenApiParameter(name='token', location=OpenApiParameter.QUERY, description='token for user activation',
                             required=True, type=str),
        ],
    )
    def get(self, request):
        try:
            token = request.GET['token']
            uid = force_str(urlsafe_base64_decode(request.GET['uibd64']))
            user = NewUser.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, NewUser.DoesNotExist) as e:
            print(uid)
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            json = {
                "msg": "activation done",
            }
            return Response(json, status=status.HTTP_200_OK)
        else:
            json = {
                "msg": "error activation",
            }
            return Response(json, status=status.HTTP_400_BAD_REQUEST)


class CustomPasswordResetView:
    @receiver(reset_password_token_created)
    def password_reset_token_created(sender, reset_password_token, *args, **kwargs):
        """
          Handles password reset tokens
          When a token is created, an e-mail needs to be sent to the user
        """
        # send an e-mail to the user
        site_url = settings.SITE_URL
        context = {
            'username': reset_password_token.user.user_name,
            'email': reset_password_token.user.email,
            'reset_password_url': "{}/password-reset?token={}".format(site_url, reset_password_token.key),
        }

        email_plaintext_message = render_to_string('blog/user_reset_password.txt', context)

        to_email = reset_password_token.user.email
        mail_subject = "Password Reset for {}".format(site_url)
        email = EmailMessage(
            mail_subject, email_plaintext_message, to=[to_email]
        )
        email.send()


class CustomPasswordTokenVerificationView(APIView):
    """
      An Api View which provides a method to verifiy that a given pw-reset token is valid before actually confirming the
      reset.
    """
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = CustomTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data['token']

        # get token validation time
        password_reset_token_validation_time = get_password_reset_token_expiry_time()

        # find token
        reset_password_token = ResetPasswordToken.objects.filter(key=token).first()

        if reset_password_token is None:
            return Response({'status': 'invalid'}, status=status.HTTP_404_NOT_FOUND)

        # check expiry date
        expiry_date = reset_password_token.created_at + timedelta(hours=password_reset_token_validation_time)

        if timezone.now() > expiry_date:
            # delete expired token
            reset_password_token.delete()
            return Response({'status': 'expired'}, status=status.HTTP_404_NOT_FOUND)

        # check if user has password to change
        if not reset_password_token.user.has_usable_password():
            return Response({'status': 'irrelevant'})

        return Response({'status': 'OK'})