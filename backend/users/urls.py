from django.urls import path, include

from .views import CustomUserCreate, BlacklistTokenUpdateView, ActivateTokenView, CustomPasswordTokenVerificationView

app_name = 'users'

urlpatterns = [
    path('create/', CustomUserCreate.as_view(), name="create_user"),
    path('logout/blacklist/', BlacklistTokenUpdateView.as_view(),
         name='blacklist'),
    path('activate', ActivateTokenView.as_view(), name='activate'),

    path('reset-password/', include('django_rest_passwordreset.urls', namespace='password_reset')),
]
